package layouts.tutorial.android.com.android06_advanced_list_view;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView selection;
    private ListView listView;
    private String[] items = {"lorem", "ipsum", "dolor", "sit", "amet", "lorem", "ipsum", "dolor", "sit", "amet", "lorem", "ipsum", "dolor", "sit", "amet"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        selection = (TextView) findViewById(R.id.selection);
        listView = (ListView) findViewById(R.id.listView);

        //listView.setAdapter(new ArrayAdapter<String>(this, R.layout.list_view_item, R.id.label, items));
        listView.setAdapter(new MyArrayAdapter(this, R.layout.list_view_item, items));
        listView.setOnItemClickListener(new MyOnItemClickListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class MyOnItemClickListener implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Toast.makeText(getApplicationContext(), getModel(position), Toast.LENGTH_SHORT).show();
        }

        private String getModel(int position){
            return (String) ((MyArrayAdapter) listView.getAdapter()).getItem(position);
        }
    }

    private class MyArrayAdapter extends ArrayAdapter {
        public MyArrayAdapter(Context context, int resource, Object[] objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //performance optimalization by usage of convertView
            View row = convertView;
            ViewWrapper wrapper = null;

            //id convertView is not null, recycle the view and do not create new one
            if(row == null){
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(R.layout.list_view_item, parent, false);

                wrapper = new ViewWrapper(row);
                row.setTag(wrapper);
            }
            else{
                wrapper = (ViewWrapper) row.getTag();
            }

            wrapper.getLabel().setText(items[position]);

            if(items[position].length() > 4){
                wrapper.getIcon().setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
            }
            else{
                wrapper.getIcon().setImageResource(R.drawable.abc_ic_go_search_api_mtrl_alpha);
            }

            return row;
        }
    }
}
