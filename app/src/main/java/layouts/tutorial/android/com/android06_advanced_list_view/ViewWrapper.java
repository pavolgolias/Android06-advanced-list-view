package layouts.tutorial.android.com.android06_advanced_list_view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewWrapper {
    private View base;
    private TextView label;
    private ImageView icon;

    public ViewWrapper(View base) {
        this.base = base;
        this.label = null;
        this.icon = null;
    }

    public TextView getLabel() {
        if(label == null){
            label = (TextView) base.findViewById(R.id.label);
        }
        return label;
    }

    public ImageView getIcon() {
        if(icon == null){
            icon = (ImageView) base.findViewById(R.id.icon);
        }
        return icon;
    }
}
